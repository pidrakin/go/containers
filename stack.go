package containers

import "fmt"

type Stack[T any] struct {
	stack []T
}

func New[T any]() *Stack[T] {
	s := &Stack[T]{
		stack: []T{},
	}
	return s
}

func (s *Stack[T]) Empty() bool {
	return len(s.stack) == 0
}

func (s *Stack[T]) Push(e T) {
	s.stack = append(s.stack, e)
}

func (s *Stack[T]) Pop() (T, error) {
	var popped T
	if s.Empty() {
		return popped, fmt.Errorf("cannot pop item from empty stack")
	}
	length := len(s.stack)
	s.stack, popped = s.stack[0:length-1], s.stack[length-1]
	return popped, nil
}

func (s *Stack[T]) Peek() (T, error) {
	var peeked T
	if s.Empty() {
		return peeked, fmt.Errorf("cannot peek item from empty stack")
	}
	length := len(s.stack)
	peeked = s.stack[length-1]
	return peeked, nil
}
