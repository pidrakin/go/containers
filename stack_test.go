package containers

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

func TestStack_Empty(t *testing.T) {
	s := Stack[int]{}
	assert.True(t, s.Empty())
}

func TestStack_Push(t *testing.T) {
	s := New[int]()
	s.Push(1)
	assert.False(t, s.Empty())
}

func TestStack_Pop(t *testing.T) {
	s := New[int]()

	t.Run("pop fail no item", func(t *testing.T) {
		assert.True(t, s.Empty())
		_, err := s.Pop()
		assert.NotNil(t, err)
	})

	t.Run("pop only item", func(t *testing.T) {
		s.Push(1)
		assert.False(t, s.Empty())
		popped, err := s.Pop()
		tests.AssertNoError(t, err)
		assert.Equal(t, 1, popped)
		assert.True(t, s.Empty())
	})

	t.Run("pop last item", func(t *testing.T) {
		s.Push(1)
		s.Push(2)
		assert.False(t, s.Empty())

		popped, err := s.Pop()
		tests.AssertNoError(t, err)
		assert.Equal(t, 2, popped)
		assert.False(t, s.Empty())

		popped, err = s.Pop()
		tests.AssertNoError(t, err)
		assert.Equal(t, 1, popped)
		assert.True(t, s.Empty())
	})
}

func TestStack_Peek(t *testing.T) {
	s := New[int]()

	t.Run("peek fail no item", func(t *testing.T) {
		_, err := s.Peek()
		assert.NotNil(t, err)
	})

	t.Run("peek only item", func(t *testing.T) {
		s.Push(1)

		peeked, err := s.Peek()
		tests.AssertNoError(t, err)
		assert.Equal(t, 1, peeked)
	})

	t.Run("peek last item", func(t *testing.T) {
		s.Push(1)
		s.Push(2)

		peeked, err := s.Peek()
		tests.AssertNoError(t, err)
		assert.Equal(t, 2, peeked)
	})
}
